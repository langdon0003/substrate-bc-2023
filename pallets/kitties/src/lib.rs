#![cfg_attr(not(feature = "std"), no_std)]

pub use pallet::*;

use frame_support::{inherent::Vec, pallet_prelude::*};
use frame_system::pallet_prelude::*;

// Define Kitties
#[derive(Clone, Encode, Decode, PartialEq, RuntimeDebug, TypeInfo)]
#[scale_info(skip_type_params(T))]
pub struct Kitty<T: Config> {
	pub dna: Vec<u8>,
	pub price: u64,
	pub gender: Gender,
	pub owner: T::AccountId,
}

// Define Gender
#[derive(Clone, Encode, Decode, PartialEq, Copy, RuntimeDebug, TypeInfo, MaxEncodedLen)]
pub enum Gender {
	Male,
	Female,
}

#[frame_support::pallet]
pub mod pallet {

	use super::*;

	#[pallet::pallet]
	#[pallet::without_storage_info]
	pub struct Pallet<T>(_);

	/// Configure the pallet by specifying the parameters and types on which it depends.
	#[pallet::config]
	pub trait Config: frame_system::Config {
		/// Because this pallet emits events, it depends on the runtime's definition of an event.
		type RuntimeEvent: From<Event<Self>> + IsType<<Self as frame_system::Config>::RuntimeEvent>;
	}

	// Define KittyId storage
	#[pallet::storage]
	#[pallet::getter(fn kitty_id)]
	pub(super) type KittyId<T: Config> = StorageValue<_, u32, ValueQuery>;

	// Define Kitties storage + OptionQuery
	#[pallet::storage]
	#[pallet::getter(fn get_kitty)]
	pub type Kitties<T: Config> = StorageMap<_, Blake2_128Concat, Vec<u8>, Kitty<T>>;

	//Define KittiesOwned storage + ValueQuery
	#[pallet::storage]
	#[pallet::getter(fn kitty_owned)]
	pub(super) type KittiesOwned<T: Config> =
		StorageMap<_, Blake2_128, T::AccountId, Vec<Vec<u8>>, ValueQuery>;

	#[pallet::event]
	#[pallet::generate_deposit(pub(super) fn deposit_event)]
	pub enum Event<T: Config> {
		Created { kitty: Vec<u8>, owner: T::AccountId },
	}

	// Errors inform users that something went wrong.
	#[pallet::error]
	pub enum Error<T> {
		DuplicateKitty,
		OverFlow,
	}

	#[pallet::call]
	impl<T: Config> Pallet<T> {
		#[pallet::call_index(0)]
		#[pallet::weight(0)]
		pub fn create_kitty(origin: OriginFor<T>, dna: Vec<u8>) -> DispatchResult {
			// Make sure the caller is from a signed origin
			let owner = ensure_signed(origin)?;

			//: generate gender
			let gender = Self::gen_gender(&dna)?;
			log::info!("gender : {:?}", gender);

			//  define new kitty

			// //  Check if the kitty does not already exist in our storage map
			// // using ensure!
			// // return DuplicateKitty if error
			let found_kitty = Kitties::<T>::get(&dna);
			// log::info!("found_kitty : {:?}", found_kitty.unwrap());

			if found_kitty.is_some() {
				return Err(Error::<T>::DuplicateKitty.into())
				// return Err(DispatchError::Other("Error::<T>::DuplicateKitty"));
			}
			// let cur_id = Self::kitty_id();
			// let new_id = cur_id.unwrap().checked_add(1).ok_or(Error::<T>::OverFlow);
			// //  Get current kitty id
			let current_id = KittyId::<T>::get();
			log::info!("current_id : {:?}", current_id);
			// //  Increase kitty Id by 1 (if overflow return OverFlow)
			let new_id = current_id.checked_add(1).ok_or(Error::<T>::OverFlow)?;
			// current_id += 1;
			log::info!("current_id + 1 : {:?}", current_id);

			// //  Append new kitty to KittiesOwned
			// let mut new_dna = KittiesOwned::<T>::get(&owner);
			// new_dna.push(dna.clone());
			// KittiesOwned::<T>::insert(&owner, new_dna.clone());

			// cach thay dung
			KittiesOwned::<T>::append(&owner, dna.clone());

			let new_kitty =
				Kitty::<T> { dna: dna.clone(), price: 0_u64, gender, owner: owner.clone() };
			// log::info!("new_kitty : {:?}", new_kitty);

			// //  Write new kitty to storage
			Kitties::<T>::insert(&new_kitty.dna, new_kitty.clone());

			// //  Write new kitty id
			KittyId::<T>::put(new_id);

			// // Deposit our "Created" event.
			Self::deposit_event(Event::Created { kitty: dna, owner });

			Ok(())
		}
	}
}

impl<T> Pallet<T> {
	fn gen_gender(dna: &Vec<u8>) -> Result<Gender, Error<T>> {
		let len = dna.len();

		return if len % 2 == 0 { Ok(Gender::Male) } else { Ok(Gender::Female) }
	}
}
